import java.util.Scanner;

public class TicTacToeGame{
    public static void main(String[] args){
		
		///////////////////////////////////////////////////////////////////
		//                       Welcoming Player                        //
		///////////////////////////////////////////////////////////////////
		System.out.println("⋇⋆✦⋆⋇⋇⋆✦⋆⋇⋇⋆✦⋆⋇⋇⋆✦⋆⋇⋇⋆✦⋆⋇⋆✦⋆⋇⋇⋆✦⋆⋇⋇⋆✦⋆⋇⋇⋆✦⋆⋇⋇⋆✦⋇⋆✦⋆");
        System.out.println("+*:ꔫ:*﹤+*:ꔫ: Welcome to TicTacToe! +*:ꔫ:*﹤+*:ꔫ:*+");
        Board board1 = new Board(); 
        boolean gameOver = false; 
        int player = 1; 
        Square playerToken = Square.X;


		///////////////////////////////////////////////////////////////////
		//                             Play                              //
		///////////////////////////////////////////////////////////////////
        while (!gameOver){
            if (player == 1){
                playerToken = Square.X;
            } else {
                playerToken = Square.O; 
            }

            Scanner scan = new Scanner (System.in);
            boolean validInput = true; 

            do{
			System.out.println("⋇⋆✦⋆⋇⋇⋆✦⋆⋇⋇⋆✦⋆⋇⋇⋆✦⋆⋇⋇⋆✦⋆⋇⋆✦⋆⋇⋇⋆✦⋆⋇⋇⋆✦⋆⋇⋇⋆✦⋆⋇⋇⋆✦⋇⋆✦⋆");
            System.out.println("\n" + board1 + "\n");
            System.out.println("             It's your turn, player " + player + "!");
            System.out.println("                       Row?");
			System.out.print("                        ");
            int rowInput = Integer.parseInt(scan.nextLine()); 
            System.out.println("                     Column?"); 
			System.out.print("                        ");
            int colInput = Integer.parseInt(scan.nextLine());
            
            if (!board1.placeToken(rowInput, colInput, playerToken)){
                System.out.println("        Your input is invalid. Try Again..."); 
                validInput = false; 
            } else {
				validInput = true; 
			}

            }   while(!validInput);

            if (board1.checkIfWinning(playerToken)){
				System.out.println("⋇⋆✦⋆⋇⋇⋆✦⋆⋇⋇⋆✦⋆⋇⋇⋆✦⋆⋇⋇⋆✦⋆⋇⋆✦⋆⋇⋇⋆✦⋆⋇⋇⋆✦⋆⋇⋇⋆✦⋆⋇⋇⋆✦⋇⋆✦⋆" + "\n");
                System.out.println("                  Player " + player +  " wins!"); 
				System.out.println("\n" + board1 + "\n");
				System.out.println("⋇⋆✦⋆⋇⋇⋆✦⋆⋇⋇⋆✦⋆⋇⋇⋆✦⋆⋇⋇⋆✦⋆⋇⋆✦⋆⋇⋇⋆✦⋆⋇⋇⋆✦⋆⋇⋇⋆✦⋆⋇⋇⋆✦⋇⋆✦⋆");
				
                gameOver = true; 
                scan.close(); // Closing scanner to avoid leaks! 
            } else if (board1.checkIfFull()){
				System.out.println("⋇⋆✦⋆⋇⋇⋆✦⋆⋇⋇⋆✦⋆⋇⋇⋆✦⋆⋇⋇⋆✦⋆⋇⋆✦⋆⋇⋇⋆✦⋆⋇⋇⋆✦⋆⋇⋇⋆✦⋆⋇⋇⋆✦⋇⋆✦⋆" + "\n");
                System.out.println("                   It's a tie!" + "\n"); 
				System.out.println("\n" + board1 + "\n");
				System.out.println("⋇⋆✦⋆⋇⋇⋆✦⋆⋇⋇⋆✦⋆⋇⋇⋆✦⋆⋇⋇⋆✦⋆⋇⋆✦⋆⋇⋇⋆✦⋆⋇⋇⋆✦⋆⋇⋇⋆✦⋆⋇⋇⋆✦⋇⋆✦⋆");
                gameOver = true; 
                scan.close(); // Closing scanner to avoid leaks! 
            } else { 
                player++; 
                if (player > 2){
                    player = 1; 
                }
            }
        }
		
		
		
		
		
    }
}