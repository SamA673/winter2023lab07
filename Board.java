public class Board{
	private Square[][] tictactoeBoard; 
	private int boardSideLength;





	///////////////////////////////////////////////////////////////////
	//                         Constructor                           //
	///////////////////////////////////////////////////////////////////
	public Board(){
		this.boardSideLength = 3; // We can make this a custom value later using a constructor input!
		this.tictactoeBoard = new Square[boardSideLength][boardSideLength]; 
		
		// Sets everything to BLANK
		for (int i = 0; i < boardSideLength; i++){
			for (int j = 0; j < boardSideLength; j++){
				tictactoeBoard[i][j] = Square.BLANK; 
			}
		}
		
		
	} 
	
	///////////////////////////////////////////////////////////////////
	//                           toString                            //
	///////////////////////////////////////////////////////////////////
	public String toString(){
		String board=""; 
		for (int i = 0; i < boardSideLength; i++){
			board += "                       ";
			for (int j = 0; j < boardSideLength; j++){
				board+= this.tictactoeBoard[i][j];
			} 
		board += '\n'; 
		} 
		return board; 
		
	}

	///////////////////////////////////////////////////////////////////
	//                          Place Token                          //
	///////////////////////////////////////////////////////////////////
	public boolean placeToken(int row, int col, Square playerToken){
		if (row > this.boardSideLength || row < 0 || col > this.boardSideLength || col < 0){
			return false;
		}
		
		if (this.tictactoeBoard[row-1][col-1] == Square.BLANK){
			this.tictactoeBoard[row-1][col-1] = playerToken; 
			return true;
		} 
		
		return false; // This should never run! 
	
	}
	
	///////////////////////////////////////////////////////////////////
	//                           Fill Check                          //
	///////////////////////////////////////////////////////////////////
	public boolean checkIfFull(){
		for (Square[] row : this.tictactoeBoard){
			for (Square Square : row){
				if (Square == Square.BLANK){
					return false; 
				}
			} 
			
		} 
		return true; 
		
	} 
	
	///////////////////////////////////////////////////////////////////
	//                      Horizontal Win Check                     //
	///////////////////////////////////////////////////////////////////
	private boolean checkIfWinningHorizontal(Square playerToken){
		for(int i = 0; i < boardSideLength; i++){
			int count = 0; 
			for(int j = 0; j < boardSideLength; j++){
				if (this.tictactoeBoard[i][j] == playerToken){
					count++; 
				}
				if (count == 3) return true; 
			}
		}
		return false; 
	}
	
	///////////////////////////////////////////////////////////////////
	//                      Vertical Win Check                       //
	///////////////////////////////////////////////////////////////////
	private boolean checkIfWinningVertical(Square playerToken){
		for (int i = 0; i < this.boardSideLength; i++){
			int count = 0; 
			for (int j = 0; j < boardSideLength; j++){
				if (this.tictactoeBoard[j][i] == playerToken){
					count++; 
				}
			}
			if (count == 3) return true; 
		}
		return false; 
	}
	
	///////////////////////////////////////////////////////////////////
	//                      Diagonal Win Check                       //
	///////////////////////////////////////////////////////////////////
	private boolean checkIfWinningDiagonal(Square playerToken){ 
		// Top left -> Bottom right
		int lCount = 0; 
		int rCount = 0;  
		for (int i = 0; i < this.boardSideLength; i++){
			if (this.tictactoeBoard[i][i] == playerToken){
				lCount++; 
			}
		}
		if (lCount == 3) return true; 

		// Top Right -> Bottom left 
		for (int i = 0, j = this.boardSideLength -1; i < this.boardSideLength && j >= 0 ; i++, j--){
			if(this.tictactoeBoard[i][j] == playerToken){
				rCount++; 
			}
		}
		if (rCount == 3) return true; 


		return false;
		}
		
	///////////////////////////////////////////////////////////////////
	//                           Win Check                           //
	///////////////////////////////////////////////////////////////////
	public boolean checkIfWinning(Square playerToken){
		if (checkIfWinningHorizontal(playerToken))return true;
		if (checkIfWinningVertical(playerToken)) return true; 
		if (checkIfWinningDiagonal(playerToken)) return true; 
		return false; 
		
	}

}